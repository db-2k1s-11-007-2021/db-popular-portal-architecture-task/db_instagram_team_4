-- READ UNCOMMITED
-- В этом аналитическом запросе точность не важна, тк выборка идет из
-- большого числа групп. Не страшно, если запрос подхватит незакомиченные изменения
-- (число студентов в какой-то из групп изменится)

--транзакция запрашивает среднее кол-во человек в группе на направлении и изменяет соотв. значение в поле у Направления
START TRANSACTION ISOLATION LEVEL READ UNCOMMITED;
WITH new_avg_count_of_students AS (
	SELECT 
		AVG(count_of_students) as value,
		directions.id as direction
	FROM groups 
		LEFT JOIN directions ON group.direction_id = directions.id
	WHERE year_of_formation > 2017 AND directions.facult_id = 1)

UPDATE directions
	SET avg_count_of_students = new_avg_count_of_students.value
	WHERE id = new_avg_count_of_students.direction;
COMMIT;

-- Пример грязного чтения (которое можно допустить):
-- 1 (другая транзакция)
UPDATE groups
	SET count_of_students = count_of_students + 1
WHERE groups.id = 1;
-- 2 (наша транзакция)
WITH new_avg_count_of_students AS (
	SELECT 
		AVG(count_of_students) as value,
		directions.id as direction
	FROM groups 
		LEFT JOIN directions ON group.direction_id = directions.id
	WHERE year_of_formation > 2017 AND directions.facult_id = 1)

UPDATE directions
	SET avg_count_of_students = new_avg_count_of_students.value
	WHERE id = new_avg_count_of_students.direction;
COMMIT;
-- 3 (другая транзакция)
ROLLBACK WORK;

-- READ COMMITED
-- Ниже мы не можем взять, тк тогда при получении количества 
-- студентов(для уменьшения) мы можем взять число, измененное
-- незакомиченной транзакцией(транзакция может оборваться, 
-- и число будет неактуальным)-избегаем грязного чтения
-- Выше нет смысла делать, при repeateable read мы не увидим закомиченные
-- updates из другой транзакции, а тогда, если пока в этой транзакции 
-- идет процесс присвоения статуса "отчислен", а в другой произошло обновление
-- количества студентов в группе, то транзакция не увидит новое число

-- транзакция отчисляет студента и обновляет счетчик студентов у группы
START TRANSACTION ISOLATION LEVEL READ COMMITED;
UPDATE students 
	SET status = "Отчислен" 
	WHERE id = 1;
UPDATE groups 
	SET count_of_students = count_of_students - 1
	WHERE id = (SELECT group_id FROM Students WHERE id = 1);
COMMIT;

-- Пример грязного чтения (которого нам надо избежать):
-- 1 (другая транзакция)
UPDATE groups
	SET count_of_students = count_of_students + 1
WHERE groups.id = 1;
-- 2 (наша транзакция)
UPDATE students 
	SET status = "Отчислен" 
	WHERE id = 1;
UPDATE groups 
	SET count_of_students = count_of_students - 1
	WHERE id = (SELECT group_id FROM Students WHERE id = 1);
COMMIT;

-- 3 (другая транзакция)
ROLLBACK WORK;

-- Пример неповторяющегося чтения (которое нам нужно)
-- 1 (часть нашей транзакции)
UPDATE students 
	SET status = "Отчислен" 
	WHERE id = 1;
-- 2 (другая транзакция)
UPDATE groups
	SET count_of_students = count_of_students + 1
WHERE groups.id = 1;
COMMIT;
-- 3 (наша транзакция)
UPDATE groups 
	SET count_of_students = count_of_students - 1
	WHERE id = (SELECT group_id FROM Students WHERE id = 1);

-- REPEATABLE READ
-- Здесь, после изменения зарплаты, происходит select-запрос (на сколько
-- процентов зарплата поднялась). Между этими запросами нельзя допустить
-- изменения зарплаты другой транзакцией, тк аналитический запрос будет
-- некорректен (к примеру, это нужно для какого-то отчета)

--транзакция повышает преподавателю зарплату и и делает аналитический запрос - на сколько процентов она повысилась
START TRANSACTION ISOLATION LEVEL REPEATEABLE READ;
WITH salary_before_increase AS (
	SELECT salary AS value
	FROM teachers 
	WHERE id = 1;
)
UPDATE teachers
	SET salary = salary + 10000
	WHERE id = 1;
SELECT 1 - (salary_before_increase.value::real / salary) FROM teachers 
	WHERE id = 1;
COMMIT;

-- пример неповторяющегося чтения (которое надо избежать)
-- 1 (наша транзакция)
WITH salary_before_increase AS (
	SELECT salary AS value
	FROM teachers 
	WHERE id = 1;
)
UPDATE teachers
	SET salary = salary + 10000
	WHERE id = 1;
-- 2 (другая транзакция)
UPDATE teachers
	SET salary = salary - 1000
	WHERE id = 1;
COMMIT;
-- 3 (наша транзакция)
SELECT 1 - (salary_before_increase.value::real / salary) FROM teachers 
	WHERE id = 1;

-- SERIALIZABLE
-- Здесь нельзя использовать изоляции ниже, тк тогда возможен сторонний 
-- insert нового направления в промежутке между удалением группы и 
-- получением аналитической информации (для postgre подошел бы и repeatable read)
START TRANSACTION ISOLATION LEVEL SERIALAZIABLE;
WITH groups_count_before_delete_direction AS (
	SELECT COUNT(*) as value
	FROM faculties
		LEFT JOIN directions ON faculties.id = directions.facult_id
		LEFT JOIN groups ON directions.id = groups.direction_id
	WHERE faculties.id = (SELECT facult_id FROM directions WHERE id = :dir_id)
)
DELETE FROM directions WHERE id = :dir_id;
-- на сколько процентов меньше стало групп у факультета после удаления 
-- направления
SELECT 1 - (COUNT(*)::real / groups_count_before_delete_direction.value)
FROM faculties
	LEFT JOIN directions ON faculties.id = directions.facult_id
	LEFT JOIN groups ON directions.id = groups.direction_id
WHERE faculties.id = (SELECT facult_id FROM directions WHERE id = :dir_id)
COMMIT;
-- Пример фантомного чтения (которое надо избежать):
-- 1 (наша транзакция)
WITH groups_count_before_delete_direction AS (
	SELECT COUNT(*) as value
	FROM faculties
		LEFT JOIN directions ON faculties.id = directions.facult_id
		LEFT JOIN groups ON directions.id = groups.direction_id
	WHERE faculties.id = (SELECT facult_id FROM directions WHERE id = :dir_id)
)
DELETE FROM directions WHERE id = :dir_id;
-- 2 (другая транзакция)
INSERT INTO directions (name, facult_id, cost_of_education, count_of_students) 
VALUES ("Программная инженерия", 1, 150000, 1000);
COMMIT;
-- 3 (наша транзакция)
SELECT 1 - (COUNT(*)::real / groups_count_before_delete_direction.value)
FROM faculties
	LEFT JOIN directions ON faculties.id = directions.facult_id
	LEFT JOIN groups ON directions.id = groups.direction_id
WHERE faculties.id = (SELECT facult_id FROM directions WHERE id = :dir_id)
COMMIT;
